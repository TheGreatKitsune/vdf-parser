'use strict';

/** 
 * @param {string} input
 * @param {RegExp} reg */
function* vdfObject(input, reg){
	const output = {};
	while(true){
		let [, key, value, objectEnd] = reg.exec(input) || [];
		if(objectEnd === '}'){ break; }
		if(key === undefined){ break; }
		if(value === undefined){ value = yield* vdfObject(input, reg); }
		output[key] = value;
		yield [key, value];
	}
	return output;
}

/** 
 * @param {Object} input
 * @param {number} tabs */
function vdfString(input, tabs = 0){
	const tab = '\t'.repeat(tabs);
	return Object.entries(input).reduce((prevString, [key, value]) => {
		let base = `${prevString + tab}"${key}"`;
		if(typeof(value) === 'object'){
			return `${base}\n${tab}{\n${vdfString(value, tabs + 1)}${tab}}\n`;
		}
		return `${base}\t\t"${value}"\n`;
	}, '');
}

module.exports = {
	/** @param {string} input */
	parse: function vdf_parse(input){
		const obj = vdfObject(input, /^(?:[\t\r\n ]*"([^"\n]*?)")(?:[\t\r\n ]*(?:"([^"\n]*?)"|{))|(?:[\t\r\n ]*(}))/gm);
		let value, done = false;
		while(!done){ ({value, done} = obj.next()); }
		return value;
	},
	/** @param {object} input */
	stringify: function vdf_stringify(input){
		return vdfString(input);
	}
}