'use strict';

const { parse, stringify } = require('./index');

const file = `"Example"
{
	"number"		"1234"
	"string"		"test string"
	"boolean"		"true"
	"object"
	{
		"number"		"789"
		"string"		"another string"
		"boolean"		"false"
		"object"
		{
			"string"		"last one"
		}
	}
	"emptyObject"
	{
	}
}`;

console.dir(parse(file), {depth: 6});
console.log(stringify({ Example: { empty: {}, is: true, things: { thinga: "32", thingb: "54" }}}));